const form = document.getElementById('form')

const validations = [
  {
    pattern: /\w{2,}/,
    elements: [document.getElementById('firstName'), document.getElementById('lastName')]
  },
  {
    pattern: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
    elements: [document.getElementById('email')]
  },
  {
    pattern: /\w+/,
    elements: [document.getElementById('password')]
  }
]

function validateInputs() {
  validations.forEach(item => {
    item.elements.forEach(element => {
      if (element.value.match(item.pattern)) {
        element.classList.remove('input-error')
      } else {
        element.classList.add('input-error')
      }
    })
  })
}

function handleFormSubmit(event) {
  event.preventDefault()
  validateInputs()
}

form.addEventListener('submit', handleFormSubmit)
